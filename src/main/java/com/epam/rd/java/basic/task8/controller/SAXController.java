package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Students;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.epam.rd.java.basic.task8.Constants.*;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public Students parse() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        saxParserFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        saxParserFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        SAXParser parser = saxParserFactory.newSAXParser(); // Noncompliant
        parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        SaxParserHandler handler = new SaxParserHandler();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        saxParser.parse(new File(xmlFileName), handler);
        return handler.getStudentContainer();
    }

    public static void fillXML(String file, Students container)
            throws FileNotFoundException, XMLStreamException {
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

        XMLStreamWriter xmlStreamWriter =
                outputFactory.createXMLStreamWriter(new FileOutputStream(file));
        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement(TAG_STUDENTS);

        xmlStreamWriter.writeAttribute("xmlns", "urn:students");
        xmlStreamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        xmlStreamWriter.writeAttribute("xsi:schemaLocation", "urn:students input.xsd");
        for (int i = 0; i < container.getStudentsList().size(); i++) {
            xmlStreamWriter.writeStartElement(TAG_STUDENT);
            xmlStreamWriter.writeStartElement(TAG_NAME);
            xmlStreamWriter.writeCharacters(container.getStudentsList().get(i).getName());
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeStartElement(TAG_SURNAME);
            xmlStreamWriter.writeCharacters(container.getStudentsList().get(i).getSurname());
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeStartElement(TAG_STUDPARAM);
            xmlStreamWriter.writeStartElement(TAG_UNIVERSITY);
            xmlStreamWriter.writeCharacters(container.getStudentsList().get(i).getParameters().getUniversity());
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeStartElement(TAG_FACULTY);
            xmlStreamWriter.writeCharacters(container.getStudentsList().get(i).getParameters().getFaculty());
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeStartElement(TAG_YEAR);
            xmlStreamWriter.writeCharacters(String.valueOf(container.getStudentsList().get(i).getParameters().getYear()));
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeStartElement(TAG_MARK);
            xmlStreamWriter.writeAttribute("gradingScale", container.getStudentsList().get(i).getMark().getGradingScale());
            xmlStreamWriter.writeCharacters(container.getStudentsList().get(i).getMark().getMark());
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeEndElement();
        }
        xmlStreamWriter.writeEndDocument();
    }

}