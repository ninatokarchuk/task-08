package com.epam.rd.java.basic.task8;

public class Constants {
    private Constants(){
    }
    public static final String TAG_STUDENTS = "students";
    public static final String TAG_STUDENT = "student";
    public static final String TAG_NAME = "name";
    public static final String TAG_SURNAME = "surname";
    public static final String TAG_STUDPARAM = "studyingParameters";
    public static final String TAG_UNIVERSITY = "university";
    public static final String TAG_FACULTY = "faculty";
    public static final String TAG_YEAR = "year";
    public static final String TAG_MARK = "mark";
}