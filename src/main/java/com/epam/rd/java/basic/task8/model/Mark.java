package com.epam.rd.java.basic.task8.model;

public class Mark {
    private String gradingScale;
    private String score;


    public String getGradingScale() {
        return gradingScale;
    }

    public void setGradingScale(String gradingScale) {
        this.gradingScale = gradingScale;
    }

    public String getMark() {
        return score;
    }

    public void setMark(String mark) {
        this.score = mark;
    }

    @Override
    public String toString() {
        return "Mark{" +
                "gradingScale='" + gradingScale + '\'' +
                ", mark=" + score +
                '}';
    }
}