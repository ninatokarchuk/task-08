package com.epam.rd.java.basic.task8.model;


import java.util.List;

public class Students {
    private List<Student>  studentsList;
    public List<Student> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(List<Student> studentsList) {
        this.studentsList = studentsList;
    }

    @Override
    public String toString() {
        return "Students{" +
                "studentsList=" + studentsList.toString() +
                '}';
    }

}
