package com.epam.rd.java.basic.task8.comparators;

import com.epam.rd.java.basic.task8.model.Student;

import java.util.Comparator;

public class SortByMark implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return o1.getMark().getMark().compareTo(o2.getMark().getMark());
    }
}
