package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.comparators.SortByMark;
import com.epam.rd.java.basic.task8.model.Mark;
import com.epam.rd.java.basic.task8.model.Student;
import com.epam.rd.java.basic.task8.model.Students;
import com.epam.rd.java.basic.task8.model.StudyingParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Constants.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public Students parse() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
		factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		XMLStreamReader parser = factory.createXMLStreamReader(new FileInputStream(xmlFileName));
		Students students = new Students();
		List<Student> studentList = new ArrayList<>();
		Student student = new Student();
		StudyingParameters parameters = new StudyingParameters();
		Mark mark;

		while (parser.hasNext()) {
			int event = parser.next();
			if (event == XMLStreamConstants.START_ELEMENT) {
				if (parser.getLocalName().equals(TAG_STUDENT)) {
					student = new Student();
				}
				switch (parser.getLocalName()) {
					case TAG_NAME:
						student.setName(parser.getElementText());
						break;
					case TAG_SURNAME:
						student.setSurname(parser.getElementText());
						break;
					case TAG_STUDPARAM:
						parameters = new StudyingParameters();
						break;
					case TAG_UNIVERSITY:
						parameters.setUniversity(parser.getElementText());
						break;
					case TAG_FACULTY:
						parameters.setFaculty(parser.getElementText());
						break;
					case TAG_YEAR:
						parameters.setYear(Integer.parseInt(parser.getElementText()));
						break;
					case TAG_MARK:
						mark = new Mark();
						mark.setGradingScale(parser.getAttributeValue(0));
						mark.setMark(parser.getElementText());
						student.setMark(mark);
						break;
					default:break;
				}
			} else if (event == XMLStreamConstants.END_ELEMENT) {
				if (parser.getLocalName().equals(TAG_STUDENT)) {
					studentList.add(student);
				} else if (parser.getLocalName().equals(TAG_STUDPARAM)) {
					student.setParameters(parameters);
				}
			}
		}
		studentList.sort(new SortByMark());
		students.setStudentsList(studentList);
		return students;
	}

}