package com.epam.rd.java.basic.task8.comparators;

import com.epam.rd.java.basic.task8.model.Student;

import java.util.Comparator;

public class SortByYear implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.getParameters().getYear() - o2.getParameters().getYear();
    }
}