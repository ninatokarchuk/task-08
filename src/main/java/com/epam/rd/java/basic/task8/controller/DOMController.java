package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.comparators.SortByName;
import com.epam.rd.java.basic.task8.model.Mark;
import com.epam.rd.java.basic.task8.model.Student;
import com.epam.rd.java.basic.task8.model.Students;
import com.epam.rd.java.basic.task8.model.StudyingParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Constants.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public Students parse() throws ParserConfigurationException, IOException, SAXException {
        Students students = new Students();
        Document document = getDocument(xmlFileName);
        Node rootNode = document.getFirstChild();//students
        NodeList childNodes = rootNode.getChildNodes();
        List<Student> studentList = new ArrayList<>();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (childNodes.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            NodeList studentChildes = childNodes.item(i).getChildNodes();
            Student student = new Student();
            parsing(studentChildes, student);
            studentList.add(student);
        }
        studentList.sort(new SortByName());
        students.setStudentsList(studentList);
        return students;
    }

    private static void parsing(NodeList studentChildes, Student student) {
        for (int j = 0; j < studentChildes.getLength(); j++) {
            if (studentChildes.item(j).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            switchForFindedTAGs(studentChildes, student, j);
        }
    }

    private static void switchForFindedTAGs(NodeList studentChildes, Student student, int j) {
        String nodeName = studentChildes.item(j).getNodeName();
        if (TAG_NAME.equals(nodeName)) {
            student.setName(studentChildes.item(j).getTextContent());
        } else if (TAG_SURNAME.equals(nodeName)) {
            student.setSurname(studentChildes.item(j).getTextContent());
        } else if (TAG_STUDPARAM.equals(nodeName)) {
            StudyingParameters studyingParameters = new StudyingParameters();
            NodeList studParChildes = studentChildes.item(j).getChildNodes();
            for (int k = 0; k < studParChildes.getLength(); k++) {
                if (studParChildes.item(k).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                String name = studParChildes.item(k).getNodeName();
                if (TAG_UNIVERSITY.equals(name)) {
                    studyingParameters.setUniversity(studParChildes.item(k).getTextContent());
                } else if (TAG_FACULTY.equals(name)) {
                    studyingParameters.setFaculty(studParChildes.item(k).getTextContent());
                } else if (TAG_YEAR.equals(name)) {
                    studyingParameters.setYear(Integer.parseInt(studParChildes.item(k).getTextContent()));
                }
            }
            student.setParameters(studyingParameters);
        } else if (TAG_MARK.equals(nodeName)) {
            Mark mark = new Mark();
            mark.setGradingScale(studentChildes.item(j).getAttributes().item(0).getTextContent());
            mark.setMark(studentChildes.item(j).getChildNodes().item(0).getTextContent());
            student.setMark(mark);
        }
    }

    private static Document getDocument(String fileName) throws SAXException, IOException, ParserConfigurationException {
        File file = new File(fileName);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        documentBuilderFactory.setNamespaceAware(true);

        documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setValidating(true);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/validation", true);
        documentBuilderFactory.setFeature("http://apache.org/xml/features/validation/schema", true);

        return documentBuilderFactory.newDocumentBuilder().parse(file);
    }

    public static void fillXml(String file, Students container) throws ParserConfigurationException, TransformerException, FileNotFoundException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        Element root = document.createElementNS("urn:students", TAG_STUDENTS);

        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "urn:students input.xsd");
        document.appendChild(root);
        Element student;
        Element name;
        Element surname;
        Element studParam;
        Element univer;
        Element faculty;
        Element year;
        Element mark;
        Text nameText;
        Text surnameText;
        Text textUniver;
        Text textFaculty;
        Text textYear;
        Text textMark;
        for (int i = 0; i < container.getStudentsList().size(); i++) {
            student = document.createElement(TAG_STUDENT);
            root.appendChild(student);
            name = document.createElement(TAG_NAME);
            student.appendChild(name);
            nameText = document.createTextNode(container.getStudentsList().get(i).getName());
            name.appendChild(nameText);
            surname = document.createElement(TAG_SURNAME);
            student.appendChild(surname);
            surnameText = document.createTextNode(container.getStudentsList().get(i).getSurname());
            surname.appendChild(surnameText);
            studParam = document.createElement(TAG_STUDPARAM);
            student.appendChild(studParam);
            univer = document.createElement(TAG_UNIVERSITY);
            textUniver = document.createTextNode(container.getStudentsList().get(i).getParameters().getUniversity());
            univer.appendChild(textUniver);
            studParam.appendChild(univer);
            faculty = document.createElement(TAG_FACULTY);
            textFaculty = document.createTextNode(container.getStudentsList().get(i).getParameters().getFaculty());
            faculty.appendChild(textFaculty);
            studParam.appendChild(faculty);
            year = document.createElement(TAG_YEAR);
            textYear = document.createTextNode(String.valueOf(container.getStudentsList().get(i).getParameters().getYear()));
            year.appendChild(textYear);
            studParam.appendChild(year);
            mark = document.createElement(TAG_MARK);
            mark.setAttribute("gradingScale", container.getStudentsList().get(i).getMark().getGradingScale());
            textMark = document.createTextNode(container.getStudentsList().get(i).getMark().getMark());
            mark.appendChild(textMark);
            student.appendChild(mark);
        }
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        tf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(document), new StreamResult(new FileOutputStream(file)));
    }


}
