package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.comparators.SortByYear;
import com.epam.rd.java.basic.task8.model.Mark;
import com.epam.rd.java.basic.task8.model.Student;
import com.epam.rd.java.basic.task8.model.Students;
import com.epam.rd.java.basic.task8.model.StudyingParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import static com.epam.rd.java.basic.task8.Constants.*;

public class SaxParserHandler extends DefaultHandler {

    private Students students = new Students();
    private List<Student> studentList = new ArrayList<>();
    private String currentTagName;
    private String name;
    private String surname;
    private boolean insideParam;
    StudyingParameters parameters = null;
    private String university;
    private String faculty;
    private int year;
    Mark mark = null;
    private String gradingScale;
    private String score;

    @Override
    public void endDocument() throws SAXException {
        studentList.sort(new SortByYear());
        students.setStudentsList(studentList);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentTagName = qName;
        if (qName.equals(TAG_STUDPARAM)) {
            insideParam = true;
        } else if (qName.equals(TAG_MARK)) {
            gradingScale = attributes.getValue(0);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals(TAG_MARK)) {
            mark = new Mark();
            mark.setGradingScale(gradingScale);
            mark.setMark(score);
        }
        if (qName.equals(TAG_STUDPARAM)) {
            parameters = new StudyingParameters();
            parameters.setUniversity(university);
            parameters.setFaculty(faculty);
            parameters.setYear(year);
            insideParam = false;
        }
        if (qName.equals(TAG_STUDENT)) {
            Student student = new Student();
            student.setName(name);
            student.setSurname(surname);
            student.setParameters(parameters);
            student.setMark(mark);
            studentList.add(student);
        }
        currentTagName = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentTagName == null) {
            return;
        }
        if (currentTagName.equals(TAG_NAME)) {
            name = new String(ch, start, length);
        } else if (currentTagName.equals(TAG_SURNAME)) {
            surname = new String(ch, start, length);
        }
        if (insideParam) {
            switch (currentTagName) {
                case TAG_UNIVERSITY:
                    university = new String(ch, start, length);
                    break;
                case TAG_FACULTY:
                    faculty = new String(ch, start, length);
                    break;
                case TAG_YEAR:
                    year = Integer.parseInt(new String(ch, start, length));
                    break;
                default:break;
            }
        }
        if (currentTagName.equals(TAG_MARK)) {
            score = new String(ch, start, length);
        }
    }

    public Students getStudentContainer() {
        return students;
    }
}
