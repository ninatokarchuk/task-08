package com.epam.rd.java.basic.task8.model;



public class Student  {
    private String name;
    private String surname;
    private StudyingParameters parameters;
    private Mark mark;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public StudyingParameters getParameters() {
        return parameters;
    }

    public void setParameters(StudyingParameters parameters) {
        this.parameters = parameters;
    }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", parameters=" + parameters +
                ", mark=" + mark +
                '}';
    }


}
